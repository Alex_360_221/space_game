// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PlanetController.generated.h"

UCLASS()
class SG_API APlanetController : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APlanetController();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//this will create the top layers of the octree
	void CreateIntialOctree();
	//spawns one chunk
	class AChunk* SpawnChunk(class OctreeNode* node, FVector chunkPos, float newVertexDistance);
	//removes the octree node at chunk pos
	void RemoveNode(FVector chunkPos);
	//adds chunk to array which will remove chunks
	void AddChunkToDestroy(class AChunk* newChunk);

	class APawn* player;

private:
	float radiusInner, radiusOuter, chunkSize, vertexDistance;
	TArray<class AChunk*> chunks;
	TMap<FVector, class OctreeNode*> octreeNodes;
	FVector planetCenter;

	float timer;

	bool spawnIntialChunks;

	TArray<class AChunk*>  chunksToDestory;

	//will sort octrees in order of closest to the player and work out distance 
	//void SortOctreeNodes(FVector playerPos);
	//will work out the distance from the node to the player and weather the node should be updated
	void CalculateNodesDistanceFromPlayer(FVector playerPos);
	//this will remove the chunks in this array
	void DestroyChunks();

	//this will return a vector which will indecate which axis should be used to uv unwrap
	FVector calculateUVUnwrapMethod(float dot, FVector direction);
	//this will work out the local up vector for a chunk
	FVector calculateLocalUpVector(float dot, FVector direction);
};
