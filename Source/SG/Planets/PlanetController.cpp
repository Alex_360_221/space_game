// Fill out your copyright notice in the Description page of Project Settings.


#include "PlanetController.h"
#include "Mesh/Chunk.h"
#include "OctreeNode.h"

// Sets default values
APlanetController::APlanetController()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	radiusInner = 4000000;
	radiusOuter = 12000000;
	chunkSize = 4000000;
	vertexDistance = 200000;
	planetCenter = FVector(0, 0, 0);

	spawnIntialChunks = false;
}

// Called when the game starts or when spawned
void APlanetController::BeginPlay()
{
	Super::BeginPlay();
	CreateIntialOctree();
}

// Called every frame
void APlanetController::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	timer += DeltaTime;
	if (timer >= 0.5f && spawnIntialChunks)
	{
		timer = 0;
		//update
		if (!player) { return; }
		//float dis = FVector::Distance(planetCenter, player->GetActorLocation());
		//GLog->Log("Distance : " + FString::FromInt(dis));
		CalculateNodesDistanceFromPlayer(player->GetActorLocation());
		DestroyChunks();
	}
}

void APlanetController::CreateIntialOctree()
{
	int halfChunkCount = int(radiusOuter / chunkSize);
	for (int z = halfChunkCount * -1; z <= halfChunkCount; z++)
	{
		for (int x = halfChunkCount * -1; x <= halfChunkCount; x++)
		{
			for (int y = halfChunkCount * -1; y <= halfChunkCount; y++)
			{
				FVector chunkPos = FVector(x * chunkSize, y * chunkSize, z * chunkSize);
				//check if chunk is within the planet radius
				float dis = FVector::Dist(chunkPos, planetCenter);
				if (dis >= radiusInner && dis <= radiusOuter)
				{
					//create Octree model
					OctreeNode* newNode = new OctreeNode(chunkPos, this, vertexDistance, 0);
					octreeNodes.Add(chunkPos, newNode);
				}
			}
		}
	}
	GLog->Log("Amount of chunks spawned for Planet " + FString::FromInt(chunks.Num()));
	spawnIntialChunks = true;
}

AChunk* APlanetController::SpawnChunk(OctreeNode* node, FVector chunkPos, float newVertexDistance)
{
	if (!node) { return nullptr; }
	UWorld* world = GetWorld();
	if (!world) { return nullptr; }
	AChunk* newChunk = world->SpawnActor<AChunk>(chunkPos, FRotator::ZeroRotator);
	//set chunk vaules
	if (newChunk)
	{
		newChunk->node = node;
		newChunk->vertexDistance = newVertexDistance;
		newChunk->planetRadius = 5000000;
		FVector normVec = chunkPos;
		normVec.Normalize();
		float dot = FVector::DotProduct(planetCenter.UpVector, normVec);
		newChunk->uvAxis = calculateUVUnwrapMethod(dot, normVec);
		newChunk->localUp = calculateLocalUpVector(dot, normVec);
		if (node->octreeLevel == 0)
		{
			newChunk->generateMasterImage = true;
		}
		newChunk->SetVaules = true;
		//add chunk to array ....might not need
		chunks.Add(newChunk);
	}
	
	//return chunk ref
	return newChunk;
}

void APlanetController::RemoveNode(FVector chunkPos)
{
	OctreeNode* currentNode = octreeNodes.FindRef(chunkPos);
	currentNode = nullptr;
	octreeNodes.Remove(chunkPos);
}

void APlanetController::AddChunkToDestroy(AChunk* newChunk)
{
	if (newChunk)
	{
		//make them invisble to svae gpu power
		//add to array for removing
		chunksToDestory.Add(newChunk);
		GLog->Log("Chunks for destorying : " + FString::FromInt(chunksToDestory.Num()));
	}
}

void APlanetController::CalculateNodesDistanceFromPlayer(FVector playerPos)
{
	TArray<OctreeNode*> nodes;
	octreeNodes.GenerateValueArray(nodes);
	GLog->Log("Amount of Active Starter Nodes " + FString::FromInt(nodes.Num()));
	for (int i = 0; i < nodes.Num();i++)
	{
		if (!nodes[i]) { return; }
		float dis = FVector::Distance(playerPos, nodes[i]->meshCentre);
		
		if (dis < 4000000 && !nodes[i]->spilt)
		{
			GLog->Log("Spilt Chunk");
			GLog->Log("Chunk distance " + FString::FromInt(dis));
			nodes[i]->SpiltOctreeNode();
		}
		/*if (dis < 7000000 + chunkSize)
		{*/
			//update node
			nodes[i]->UpdateOctreeNode(playerPos);
		//}
		//else if(dis > (7000000 + chunkSize) && nodes[i]->spilt)
		//{
			//remove nodes
		//}
		
	}
}

void APlanetController::DestroyChunks()
{
	int maxCount = 100;
	if (chunksToDestory.Num() < maxCount) { maxCount = chunksToDestory.Num(); }
	//uses a limit of max count so not all the chunks are destroyed in one tick
	for (int i = 0; i < maxCount; i++)
	{
		if (chunksToDestory[0]) { chunksToDestory[0]->DestroyChunk(); }
		chunksToDestory.RemoveAt(0);
	}
	GLog->Log("Chunks for destorying : " + FString::FromInt(chunksToDestory.Num()));
}

FVector APlanetController::calculateUVUnwrapMethod(float dot, FVector direction)
{
	if (dot > 0.5 || dot < -0.5) { return FVector(1, 1, 0); }
	else
	{
		if (direction.X > 0.5 || direction.X < -0.5) { return FVector(0, 1, 1); }
		return FVector(1, 0, 1);
	}
	return FVector(0,0,0);
}

FVector APlanetController::calculateLocalUpVector(float dot, FVector direction)
{
	if (dot > 0.5) { return FVector(0, 0, 1); }
	else if (dot < -0.5) { return FVector(0, 0, -1); }
	else
	{
		if (direction.X > 0.5) { return FVector(1, 0, 0); }
		else if (direction.X < -0.5) { return FVector(-1, 0, 0); }
		if (direction.Y > 0.5) { return FVector(0, 1, 0); }
		else if (direction.Y < -0.5) { return FVector(0, -1, 0); }
		return FVector(0, 0, 0);
	}
	return FVector(0, 0, 0);
}

