// Fill out your copyright notice in the Description page of Project Settings.


#include "Chunk.h"
#include "MarchingCubesGeneration.h"
#include <SG/Planets/OctreeNode.h>

#include "Image/GenerateMasterImage.h"

// Sets default values
AChunk::AChunk(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	vertexDistance = 200000;
	vertexCount = 20;
	//zVertexCount = vertexCount;
	//zVertexDistance = 160000;
	generatedMeshVaules = false;
	createdMesh = false;
	callGenerateMeshVaules = false;
	destorySelf = false;
	generateMasterImage = false;
	generatedMasterImage = false;
	generatingMasterImage = false;
	createMasterImage = false;
	planetRadius = 5000000;

	mesh = CreateDefaultSubobject<UProceduralMeshComponent>(TEXT("Mesh"));
	mesh->SetupAttachment(RootComponent);
	SetRootComponent(mesh);

	static ConstructorHelpers::FObjectFinder<UMaterialInterface>Material1(TEXT("MaterialInstanceConstant'/Game/Mats/Planet_Material/Planet_Material_Inst.Planet_Material_Inst'"));	
	materialInterface = Material1.Object;
}

// Called when the game starts or when spawned
void AChunk::BeginPlay()
{
	Super::BeginPlay();
	terrainMaterial = UMaterialInstanceDynamic::Create(materialInterface, this);
	//if (parent) { zVertexCount = parent->zVertexCount; }
	//else { zVertexCount = vertexCount; }

}

// Called every frame
void AChunk::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (SetVaules)
	{
		if (generateMasterImage && !generatingMasterImage)
		{
			generatingMasterImage = true;

			float lr = 0;
			float hr = (planetRadius * 4);
			//to get rid of negatives add half radius
			float p = hr - (GetActorLocation().X + (hr / 2));
			midPointX = (hr - p) / hr;
			float halfRange = ((vertexCount / 2) * vertexDistance) / hr;
			startX = midPointX - halfRange;
			if (startX < 0) { startX = 0; }
			endX = midPointX + halfRange;
			if (endX > 1) { endX = 1; }
			range = endX - startX;

			//generate master image
			int imageVertexCount = ((vertexCount+2) *10)/* + (2 * 2)*/;
			(new FAutoDeleteAsyncTask<GenerateMasterImage>(
				this, GetActorLocation(), imageVertexCount, (vertexDistance / 10.f), 5000000, FVector(0, 0, 0),localUp
				))->StartSynchronousTask();
		}
		if (generatedMasterImage  && !createMasterImage)
		{
			createMasterImage = true;

			//TArray<FColor> colorData;
			/*for (int i = 0; i < 4; i++)
			{
				colourData.Add(FColor(255, 0, 0, 255));
			}*/
			if (colourData.Num() == 0) { return; }
			GLog->Log("Create Image with image data of : " + FString::FromInt(colourData.Num()));
			int imageRes = ((vertexCount+2) * 10) /*+ (2 * 2)*/;
			masterTerrainImage = UTexture2D::CreateTransient(imageRes, imageRes, PF_R8G8B8A8);
			masterTerrainImage->NeverStream = false;
			//masterTerrainImage->bNoTiling = true;
			masterTerrainImage->SRGB = 0;
			//FCreateTexture2DParameters textureParamerters = FCreateTexture2DParameters();
			//highResTexture = FImageUtils::CreateTexture2D(imageRes, imageRes, colourData,this,"texure",
			//	EObjectFlags::RF_Dynamic, textureParamerters);

			FTexture2DMipMap& mip = masterTerrainImage->PlatformData->Mips[0];
			void* mipdata = mip.BulkData.Lock(LOCK_READ_WRITE);
			FMemory::Memcpy(mipdata, colourData.GetData(), imageRes * imageRes * 4);
			mip.BulkData.Unlock();
			masterTerrainImage->UpdateResource();
			terrainMaterial->SetTextureParameterValue("TerrainImage", masterTerrainImage);
			terrainMaterial->PostEditChange();
			mesh->SetMaterial(0, terrainMaterial);

		}

		if (!callGenerateMeshVaules)
		{
			callGenerateMeshVaules = true;
			(new FAutoDeleteAsyncTask<MarchingCubesGeneration>(this, GetActorLocation(), vertexCount, vertexCount, vertexCountFull
				, vertexDistance, vertexDistance, 4, uvAxis))->StartBackgroundTask();
		}
		if (generatedMeshVaules && !createdMesh)
		{
			if (destorySelf) { Destroy(); }//this chunk should be destroyed so no need to gen mesh
			GLog->Log("Gen Mesh " + FString::FromInt(vertices.Num()));
			createdMesh = true;
			TArray <FColor> VertexColour;
			mesh->ClearMeshSection(0);
			mesh->CreateMeshSection(0, vertices, triangles, normals, UVs, VertexColour, Tangents, false);
			mesh->SetMaterial(0, terrainMaterial);
			mesh->CastShadow = false;

			GLog->Log("Vertex count = " + FString::FromInt(vertices.Num()) + "UVs count = " + FString::FromInt(UVs.Num()));
			if(node)
			{		
				if (vertices.Num() <= 0) { /*node->RemoveNode();*/ }
				else { node->meshCentre = meshCentre; }
				node->CompletedChunkSpawn();
			}
			
			/*if (toggleMat) { terrainMesh->SetMaterial(0, terrainMat); }
			else { terrainMesh->SetMaterial(0, normalsMat); }*/
			//vertices.Empty();
			//triangles.Empty();
			//normals.Empty();
			//UVs.Empty();
			//Tangents.Empty();

		}
	}

}

void AChunk::DestroyChunk()
{
	//if mesh has been calculated destory mesh stright away
	//if not tell it to destroy after mesh gen been done
	if (createdMesh) { Destroy(); }
	else { destorySelf = true; }
}

