// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ProceduralMeshComponent.h"
#include "Chunk.generated.h"

UCLASS()
class SG_API AChunk : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AChunk(const FObjectInitializer& ObjectInitializer);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		UProceduralMeshComponent* mesh;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInterface* materialInterface;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		UMaterialInstanceDynamic* terrainMaterial;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FVector4> cells;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float planetRadius;
//image varaibles
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		class UTexture2D* masterTerrainImage;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector uvAxis;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector localUp;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float midPointX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float startX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float endX;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		float range;
	//UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		TArray<FColor> colourData;

	bool generateMasterImage, generatedMasterImage, generatingMasterImage, createMasterImage;

//back to chunk variables
	TArray<FVector> vertices;
	TArray<int> triangles;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		TArray<FVector2D> UVs;
	TArray<FVector> normals;
	TArray<FProcMeshTangent> Tangents;
	TArray<FColor> vertexColour;

	float vertexDistance;
	int vertexCount;
	int vertexCountFull;
	//int zVertexCount;
	//int zVertexDistance;
	bool generatedMeshVaules;
	bool createdMesh;
	bool SetVaules, callGenerateMeshVaules;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		bool destorySelf;
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
		FVector meshCentre;

	class OctreeNode* node;

	void DestroyChunk();
};
