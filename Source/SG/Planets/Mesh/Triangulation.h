// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class SG_API Triangulation
{
public:
	Triangulation();
	static void CalculateTriangulation(int index, int verticesInLIne, TArray<int>* outArray);
	static void CalculateMarchingCubesTriangulation(FVector cellPos, float cellDistance, int cellsInLine,
		TMap<FVector, float>* cells, TArray<FVector>* vertices, TArray<int>* triangles, TArray<FVector2D>* uvs, bool addToNormalIndices, 
		TArray<int>* normalIndices, TMap<FVector, FColor>* colour, TArray<FColor>* vertexColour, FVector uvAxis);

private:
	static FVector4 CellData(TMap<FVector, float>* cells, FVector cellPos);
	static FVector InterpolateVertices(FVector4 v1, FVector4 v2);
	static void MarchingCubesUnWrap(FVector v1, FVector v2, FVector v3, TArray<FVector2D>* uvs, float uvScale);
	//this will choose the correct cords depending on the chunk
	static FVector2D Unwrap(FVector pos, float uvScale, FVector uvAxis);
};
