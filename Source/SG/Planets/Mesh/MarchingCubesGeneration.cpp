// Fill out your copyright notice in the Description page of Project Settings.


#include "MarchingCubesGeneration.h"
#include "KismetProceduralMeshLibrary.h"
#include "ProceduralMeshComponent.h"
#include "Runtime/Core/Public/Async/ParallelFor.h"
#include "Chunk.h"
#include "Triangulation.h"
#include "FastNoise.h"

MarchingCubesGeneration::MarchingCubesGeneration(class AChunk* chunk, FVector chunkPos, int vertexCount,
	int zVertexCount, int vertexCountFull, float vertexDistance, float zVertexDistance, int extraVertices, FVector uvAxis)
{
	this->chunk = chunk;
	this->chunkPos = chunkPos;
	this->vertexCount = vertexCount;
	this->zVertexCount = zVertexCount;
	this->vertexCountFull = vertexCountFull;
	this->vertexDistance = vertexDistance;
	this->zVertexDistance = zVertexDistance;
	this->extraVertices = extraVertices;
	this->uvAxis = uvAxis;
}


void MarchingCubesGeneration::CreateMarchingCubesMesh(/*class UProceduralMeshComponent* mesh, UMaterialInterface* material, FVector chunkPos, int vertexCount
	, int zVertexCount,int vertexCountFull, float vertexDistance, float zVertexDistance, int extraVertices*/)
{
	TMap<FVector, float> cells;
	TMap<FVector, FColor> colour;
	TArray<FVector4> cellData;
	TArray<FVector> vertices;
	TArray<FColor> vertexColour;
	TArray<int> triangles;
	TArray<FVector2D> UVs;
	TArray<FVector> vertices_Full;
	TArray<int> triangles_Full;
	TArray<FVector2D> UVs_Full;
	TArray<int> normalsIndices;
	/*FastNoise fn;
	fn.SetNoiseType(FastNoise::Perlin);
	fn.SetSeed(74874);*/
	//FVector chunkPos = GetActorLocation();
	int halfVertexCount = (vertexCount + 2) / 2;
	int extravertices = 2;
	//stage one is to create all the cell data
	int zCount = vertexCount + (extraVertices * 2) /*- 1*/;
	int xyCount = (halfVertexCount * 2) + (extraVertices * 2)/* - 1*/;
	int xyzCount = (xyCount * xyCount) * zCount;


	//intilise all the noise settings
	/*for (int i = 0; i < noiseSettings.Num();i++)
	{*/
		FastNoise* fn = new FastNoise();
		fn->SetNoiseType(FastNoise::Perlin);
		//fn->SetFractalOctaves(noiseSettings[i]->octaves);
		//fn->SetFrequency(noiseSettings[i]->frequency);
		fn->SetSeed(74874);
		noise.Add(fn);
	//}

	//create cell data
	FCriticalSection mutex;
	ParallelFor(xyzCount, [&](int32 index)
	{
		//work out the z x y pos
		int z = index / (xyCount * xyCount);
		int zl = index - (z * (xyCount * xyCount));
		int x = zl / xyCount;
		int y = zl - (x * xyCount);

		//minus vaules from vaule to get the negtive numbers
		z -= halfVertexCount + extravertices;
		x -= halfVertexCount + extravertices;
		y -= halfVertexCount + extravertices;

		FVector cellPos = FVector(x * vertexDistance, y * vertexDistance, z * vertexDistance);
		FVector cell = cellPos;
		//calcualte and add cell desnity 
		mutex.Lock();
		cells.Add(cell, CalculateDesnity(cellPos, chunkPos, vertexDistance, vertexDistance, extraVertices, fn));
		mutex.Unlock();
	});

	for (int z = (halfVertexCount + extravertices) * -1; z < (halfVertexCount-1 + extravertices); z++)
	{
		for (int x = (halfVertexCount + extravertices) * -1; x < (halfVertexCount-1 + extravertices); x++)
		{
			for (int y = (halfVertexCount + extravertices) * -1; y < (halfVertexCount-1 + extravertices); y++)
			{
				bool addedToMain = false;
				if (x > halfVertexCount * -1 && x <= halfVertexCount - 2 && y > halfVertexCount * -1 && y <= halfVertexCount - 2
					&& z > halfVertexCount * -1 && z <= halfVertexCount - 2)
				{
					addedToMain = true;
					Triangulation::CalculateMarchingCubesTriangulation(FVector(x * vertexDistance, y * vertexDistance, z * vertexDistance), vertexDistance
						, vertexCount, &cells, &vertices, &triangles, &UVs, false, &normalsIndices, &colour, &vertexColour, uvAxis);
				}
				if (x < (halfVertexCount + extravertices) - 1 && y < (halfVertexCount + extravertices) - 1
					&& z < (halfVertexCount + extravertices) - 1)
				{
					Triangulation::CalculateMarchingCubesTriangulation(FVector(x * vertexDistance, y * vertexDistance, z * vertexDistance), vertexDistance
						, vertexCount + 4, &cells, &vertices_Full, &triangles_Full, &UVs_Full, addedToMain, &normalsIndices, &colour, &vertexColour, uvAxis);
				}
			}
		}
	}
	TArray<FVector> normals_Full;
	TArray<FProcMeshTangent> Tangents_Full;
	TArray<FVector> normals;
	TArray<FProcMeshTangent> tangents;
	TArray <FColor> VertexColour;
	UKismetProceduralMeshLibrary::CalculateTangentsForMesh(vertices_Full, triangles_Full, UVs_Full, normals_Full, Tangents_Full);
	for (int i = 0; i < normalsIndices.Num();i++)
	{
		normals.Add(normals_Full[normalsIndices[i]]);
		tangents.Add(Tangents_Full[normalsIndices[i]]);
	}
	//calculate mesh centre
	FVector meshCentre;
	FVector v = FVector::ZeroVector;
	for (int i = 0; i < vertices.Num();i++)
	{
		v += vertices[i];
	}
	meshCentre = (v / vertices.Num()) + chunkPos;


	//mesh->CreateMeshSection(0, vertices, triangles, normals, UVs, VertexColour, tangents, true);
	//terrainMesh->CreateMeshSection(0, vertices_Full, triangles_Full, normals_Full, UVs_Full, VertexColour, Tangents_Full, true);
	//mesh->SetMaterial(0, material);
	chunk->vertices = vertices;
	chunk->triangles = triangles;
	chunk->normals = normals;
	chunk->UVs = UVs;
	chunk->Tangents = tangents;
	chunk->meshCentre = meshCentre;
	chunk->generatedMeshVaules = true;
}

void MarchingCubesGeneration::DoWork()
{
	CreateMarchingCubesMesh();
}

float MarchingCubesGeneration::CalculateDesnity(FVector cellPos, FVector ChunkPos, float VertexDistance, float ZVertexDistance, int ExtraVertices, FastNoise* fn)
{
	//work out density
	float data = 0.f;
	//base layer
	data += (fn->GetNoise(((cellPos.X + ChunkPos.X) * 0.0005), ((cellPos.Y + ChunkPos.Y) * 0.0005)/*, (cellPos.Z + ChunkPos.Z) * 0.0005*/)) * 10000;
	//base layer
	data += (fn->GetNoise(((cellPos.X + ChunkPos.X) * 0.0001), ((cellPos.Y + ChunkPos.Y) * 0.0001)/*, (cellPos.Z + ChunkPos.Z) * 0.0001*/)) * 1000;

	data += 5000000;
	////}
	////cellPos.Z = data;
	////surface *= 4000.0f;
	/*float rf = 0;
	float surface = data;
	float cellHeight = FVector::Distance(cellPos + ChunkPos, FVector(0, 0, 0))- 5000000;
	if (cellHeight > 0.0f)
	{
		rf = surface / cellHeight;
		if (rf < 0) { rf *= -1; }
	}*/

	float dis = ((cellPos + ChunkPos) - FVector(0, 0, 0)).Size() - data;
	float sphere = 0.f;
	if (dis < 0) { sphere = FGenericPlatformMath::Min(1.0f, (data - dis) / VertexDistance); }
	else { sphere = FGenericPlatformMath::Max(-1.0f, -(dis - data) / VertexDistance); }
	return sphere;
	/*float dis1 = (cellPos+ChunkPos - FVector(0, 0, 800)).Size();
	float dis = ((cellPos + ChunkPos) - FVector(0,0,0)).Size() - 5000000;
	if (dis < 0) { return FGenericPlatformMath::Min(1.0f, (5000000 - dis) / VertexDistance); }
	else { return FGenericPlatformMath::Max(-1.0f, -(dis - 5000000) / VertexDistance); }*/
//	return rf;
}