// Fill out your copyright notice in the Description page of Project Settings.


#include "GenerateMasterImage.h"
#include <SG/Planets/Mesh/FastNoise.h>
#include "ColourPicker.h"
#include <SG/Planets/Mesh/Chunk.h>
#include "Math/UnrealMathUtility.h"
#include "Kismet/KismetMathLibrary.h"

GenerateMasterImage::GenerateMasterImage(class AChunk* chunk, FVector chunkPos, int vertexCount, float vertexDistance, float planetRadius, FVector planetCenter,
	FVector localUp)
{
	this->chunk = chunk;
	this->chunkPos = chunkPos;
	this->vertexCount = vertexCount;
	this->vertexDistance = vertexDistance;
	this->planetRadius = planetRadius;
	this->planetCenter = planetCenter;
	this->localUp = localUp;
}

void GenerateMasterImage::DoWork()
{
	//First stage create the colour array 
	TArray<FColor> colourData;
	//create noise 
	FastNoise* fn1 = new FastNoise();
	fn1->SetNoiseType(FastNoise::Perlin);
	fn1->SetSeed(74874);
	FastNoise* fn2 = new FastNoise();
	fn2->SetNoiseType(FastNoise::Perlin);
	fn2->SetSeed(21344);
	FastNoise* fn3 = new FastNoise();
	fn3->SetNoiseType(FastNoise::Perlin);
	fn3->SetSeed(6764);

	//do calculations so x&y match chunk calcualtions - more for noise so chunk lines up, middle of chunk is at middle of mesh
	int halfCount = (vertexCount) / 2;

	//work out the range on this chunk on the unit sphere

	float lr = 0;
	float hr = (planetRadius * 4);

	FVector2D cords = GetCords();

	//to get rid of negatives add half radius
	float px = hr - (cords.X + (hr / 2));
	float py = hr - (cords.Y + (hr / 2));
	float midPointX = (hr - px) / hr;
	float midPointY = (hr - py) / hr;
	float halfRangeA = (((vertexCount /*+ 1*/) / 2) * vertexDistance) / hr;
	float halfRangeB = ((vertexCount / 2) * vertexDistance) / hr;
	float startX =  midPointX - halfRangeA;
	//if (startX < 0) { startX = 0; }
	float endX = midPointX + halfRangeB;
	//if (endX > 1) { endX = 1; }
	GLog->Log("midpint x : " + FString::FromInt(midPointX));
	GLog->Log("Start x : " +FString::FromInt(startX));
	GLog->Log("end x : " +FString::FromInt(endX));


	float startY = midPointY - halfRangeA;
	//if (startY < 0) { startY = 0; }
	float endY = midPointY + halfRangeB;
	//if (endY > 1) { endY = 1; }

	float range = endX - startX;
	int totalVertices = vertexCount / range;
	FVector axisA = FVector(localUp.Y, localUp.Z, localUp.X);
	FVector axisB = FVector::CrossProduct(localUp, axisA);

	int lowerBoundX = (totalVertices)*startX;
	int higherBoundX = (totalVertices)*endX;
	int lowerBoundY = (totalVertices)*startY;
	int higherBoundY = (totalVertices)*endY;

	/*if (lowerBoundX == higherBoundX || lowerBoundY == higherBoundY) { return; }*/
	for (int x = higherBoundX; x >= lowerBoundX; x--)
	{
		for (int y = lowerBoundY; y < higherBoundY; y++)
		{			
			FVector2D pos = FVector2D(x * vertexDistance, y * vertexDistance);

			FVector2D percent = FVector2D(x, y) / totalVertices;
			//percent.X = percent.X + startX;
			//percent.Y = percent.Y + startY;
			//FVector pointOnUnitCube = localUp + (percent.X - .5f) * 2 * axisA + (percent.Y - .5f) * 2 * axisB;
			//if not this mesh use different local up to calucalte vertex position
			FVector pointOnUnitCube;
			if (x >= 0 && x < totalVertices && y >= 0 && y < totalVertices)
			{
				pointOnUnitCube = localUp + (percent.X - .5f) * 2 * axisA + (percent.Y - .5f) * 2 * axisB;
			}
			else
			{
				//pointOnUnitCube = localUp + (percent.X - .5f) * 2 * axisA + (percent.Y - .5f) * 2 * axisB;
				pointOnUnitCube = CalculatePointOnOtherUnitSphere(x, y, percent, (1.f / totalVertices), /*localUp, */totalVertices);
			}

			float x2 = pointOnUnitCube.X * pointOnUnitCube.X;
			float y2 = pointOnUnitCube.Y * pointOnUnitCube.Y;
			float z2 = pointOnUnitCube.Z * pointOnUnitCube.Z;
			FVector s;
			s.X = pointOnUnitCube.X * FMath::Sqrt(1.f - (y2 / 2.f) - (z2 / 2.f) + ((y2 * z2) / 3.f));
			s.Y = pointOnUnitCube.Y * FMath::Sqrt(1.f - (x2 / 2.f) - (z2 / 2.f) + ((z2 * x2) / 3.f));
			s.Z = pointOnUnitCube.Z * FMath::Sqrt(1.f - (x2 / 2.f) - (y2 / 2.f) + ((x2 * y2) / 3.f));

			FVector pointOnUnitSphere = s;

			FVector pointOnPlanet = pointOnUnitSphere * planetRadius;

			//generate 3 different plans of noise vaules
			float elevation = (fn1->GetNoise(((pointOnPlanet.X) * 0.00005), ((pointOnPlanet.Y) * 0.00005),pointOnPlanet.Z * 0.00005)) * 10000;
		//	FVector point = pointOnUnitCube * 3.3 + planetCenter;
			/*float v = (fn1->GetNoise(pointOnUnitCube.X, pointOnUnitCube.Y, pointOnUnitCube.Z)) * 4;
			float elevation = (v + 1) * .5f * 1;
			elevation = FMath::Max<float>(0, elevation - 1.2f) * 0.3;
			elevation = planetRadius * (1 + elevation);
			float height = FVector::Distance(pointOnUnitCube * elevation, planetCenter);*/
			colourData.Add(ColourPicker::CalculateColour(elevation));
			//colourData.Add(FColor(height,0,0,0));
		}
	}
	GLog->Log("returningigjniooighogjgo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
	chunk->colourData = colourData;

	//set vaules can now return
	chunk->generatedMasterImage = true;
}

FVector2D GenerateMasterImage::GetCords()
{
	if (localUp == FVector(0, 0, 1)) { return FVector2D(chunkPos.X, chunkPos.Y); }
	if (localUp == FVector(0, 0, -1)) { return FVector2D(chunkPos.X, chunkPos.Y); }
	if (localUp == FVector(0, 1, 0)) { return FVector2D(chunkPos.X, chunkPos.Z); }
	if (localUp == FVector(0, -1, 0)) { return FVector2D(chunkPos.X, chunkPos.Z); }
	if (localUp == FVector(1, 0, 0)) { return FVector2D(chunkPos.Y, chunkPos.Z); }
	if (localUp == FVector(-1, 0, 0)) { return FVector2D(chunkPos.Y, chunkPos.Z); }
	return FVector2D();
}

float GenerateMasterImage::CalculateNoise(FastNoise* fn, FVector pos)
{
	float data = 0.f;
	//base layer
	data = (fn->GetNoise(((pos.X) * 0.0005), ((pos.Y) * 0.0005))) * 10000;
	//base layer
	//data += (fn->GetNoise(((pos.X) * 0.1), ((pos.Y) * 0.1))/*, (pos.Z) * 0.0001)*/) * 1000;
	return data;
}

FVector GenerateMasterImage::CalculatePointOnOtherUnitSphere(int x, int y, FVector2D percent, float sVaule, int totalVertexCount)
{
	//work out what localup vector to use
	FVector localLocalUp = localUp;
	FVector2D localPercent = percent;
	FQuat rotation = FQuat();
	FVector axisA = FVector(localUp.Y, localUp.Z, localUp.X);
	FVector axisB = FVector::CrossProduct(localUp, axisA);

	if (localUp == FVector::UpVector)
	{//up mesh
		if (x < 0) { return PointOnOtherUnitSphere(FVector::LeftVector, FVector2D(percent.Y, sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::RightVector, FVector2D(1 - percent.Y, sVaule)); }
		//if (x < 0) { return PointOnOtherUnitSphere(FVector::RightVector, FVector2D(1 - percent.Y, sVaule)); }
		//if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::LeftVector, FVector2D(percent.Y, sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::ForwardVector, FVector2D(1 - sVaule, 1 - percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::BackwardVector, FVector2D(sVaule, 1 - percent.X)); }
		//if (y > totalVertexCount - 1 ) { return PointOnOtherUnitSphere(FVector::ForwardVector, FVector2D(1 - sVaule, 1 - percent.X)); }
		//if (y < 0) { return PointOnOtherUnitSphere(FVector::BackwardVector, FVector2D(sVaule, 1 - percent.X)); }
	}
	else if (localUp == FVector::DownVector)
	{//Down mesh
		if (x < 0) { return PointOnOtherUnitSphere(FVector::RightVector, FVector2D(1 - percent.Y, 1 - sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::LeftVector, FVector2D(percent.Y, 1 - sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::ForwardVector, FVector2D(sVaule, percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::BackwardVector, FVector2D(1 - sVaule, percent.X)); }
	}
	else if (localUp == FVector::LeftVector)
	{//left mesh
		if (x < 0) { return PointOnOtherUnitSphere(FVector::ForwardVector, FVector2D(1 - percent.Y, 1 - sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::BackwardVector, FVector2D(percent.Y, 1 - sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::UpVector, FVector2D(sVaule, percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::DownVector, FVector2D(1 - sVaule, percent.X)); }
	}
	else if (localUp == FVector::RightVector)
	{//right Mesh
		if (x < 0) { return PointOnOtherUnitSphere(FVector::BackwardVector, FVector2D(percent.Y, sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::ForwardVector, FVector2D(1 - percent.Y, sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::UpVector, FVector2D(1 - sVaule, 1 - percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::DownVector, FVector2D(sVaule, 1 - percent.X)); }

	}
	else if (localUp == FVector::ForwardVector)
	{//forward Mesh
		if (x < 0) { return PointOnOtherUnitSphere(FVector::DownVector, FVector2D(percent.Y, sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::UpVector, FVector2D(1 - percent.Y, sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::RightVector, FVector2D(1 - sVaule, 1 - percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::LeftVector, FVector2D(sVaule, 1 - percent.X)); }
	}
	else if (localLocalUp == FVector::BackwardVector)
	{
		if (x < 0) { return PointOnOtherUnitSphere(FVector::UpVector, FVector2D(1 - percent.Y, 1 - sVaule)); }
		if (x > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::DownVector, FVector2D(percent.Y, 1 - sVaule)); }
		if (y < 0) { return PointOnOtherUnitSphere(FVector::RightVector, FVector2D(sVaule, percent.X)); }
		if (y > totalVertexCount - 1) { return PointOnOtherUnitSphere(FVector::LeftVector, FVector2D(1 - sVaule, percent.X)); }
	}
	return localUp + (percent.X - .5f) * 2 * axisA + (percent.Y - .5f) * 2 * axisB;
}

FVector GenerateMasterImage::PointOnOtherUnitSphere(FVector localLocalUp, FVector2D localPercent)
{
	FVector localAxisA = FVector(localLocalUp.Y, localLocalUp.Z, localLocalUp.X);
	FVector localAxisB = FVector::CrossProduct(localLocalUp, localAxisA);
	return localLocalUp + (localPercent.X - .5f) * 2 * localAxisA + (localPercent.Y - .5f) * 2 * localAxisB;
}
