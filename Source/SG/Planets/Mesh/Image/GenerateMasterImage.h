// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/Texture2D.h"
#include "Kismet/GameplayStatics.h"
#include "Runtime/Core/Public/HAL/Runnable.h"
#include "Runtime/Core/Public/Async/AsyncWork.h"

/**
 * 
 */
class SG_API GenerateMasterImage : public FNonAbandonableTask
{
	friend class FAutoDeleteAsyncTask <GenerateMasterImage>;
public:
	//pass in vertex count and vertex distance at correct distances for image
	GenerateMasterImage(class AChunk* chunk, FVector chunkPos, int vertexCount, float vertexDistance, float planetRadius, FVector planetCenter,
		FVector localUp);

	/*This function is executed when we tell our task to execute*/
	void DoWork();
private:
	class AChunk* chunk;
	FVector chunkPos;
	int vertexCount;
	float vertexDistance;
	float planetRadius;
	FVector planetCenter;
	FVector localUp;

	FVector2D GetCords();
	float CalculateNoise(class FastNoise* fn, FVector pos);
	FVector CalculatePointOnOtherUnitSphere(int x, int y, FVector2D percent, float sVaule, int totalVertexCount);
	FVector PointOnOtherUnitSphere(FVector localLocalUp, FVector2D localPercent);
	/*This function is needed from the API of the engine.
My guess is that it provides necessary information
about the thread that we occupy and the progress of our task*/
	FORCEINLINE TStatId GetStatId() const
	{
		RETURN_QUICK_DECLARE_CYCLE_STAT(TextureCreator, STATGROUP_ThreadPoolAsyncTasks);
	}
};
