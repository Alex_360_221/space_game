// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class SG_API ColourPicker
{
public:
	ColourPicker();

	//this will work out the colour for this position
	static FColor CalculateColour(float noiseVaule);

};
