// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"

/**
 * 
 */
class SG_API OctreeNode
{
public:
	OctreeNode();
	OctreeNode(FVector newNodePos, class APlanetController* newPlanetController, float newNodeVertexDistance, int newOctreeLevel);
	OctreeNode(FVector newNodePos, APlanetController* newPlanetController, OctreeNode* newParentNode, float newNodeVertexDistance
		, int newOctreeLevel, int newNodeIndex, float newNodeTargetDistance);
	FVector nodePos, meshCentre;
	class AChunk* nodeChunk;
	class TMap<int, OctreeNode*> nodes;
	int nodeIndex;
	class APlanetController* planetController;
	class OctreeNode* parentNode;
	float nodeVertexDistance;
	int octreeLevel;
	float nodeTargetDistance;
	bool spilt;
	int spawnedChunksCompleted;
	bool nodeHasChunk, nodeChunkFinished, nodeMarkedForRemoving;

	////image varaibles
	//UPROPERTY(EditAnywhere, BlueprintReadWrite)
	//	class UTexture2D* masterTerrainImage;
	//bool generateMasterImage, generatedMasterImage;

	//this will remove this chunk and this node
	void RemoveNode();
	//this will remove all the nodes
	void RemoveAllNodes();
	//this will remove the child node from the parent array
	void RemoveNodeFromParent(int removeNodeIndex);
	//this will spilt this octree node
	void SpiltOctreeNode();
	//this will update the octree nodes called on a tick rate
	void UpdateOctreeNode(FVector playerPos);
	//this will remove a LOD
	void RemoveLOD();

private:
	//this will spawn the chunk at this nodes position
	void SpawnChunk();

public:
	//this adds one to the spawned chunks completed
	void CompletedChunkSpawn() {
		if (parentNode) { parentNode->spawnedChunksCompleted += 1; } nodeChunkFinished = true;
	}
};

