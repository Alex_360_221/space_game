// Fill out your copyright notice in the Description page of Project Settings.


#include "OctreeNode.h"
#include "Mesh/Chunk.h"
#include "PlanetController.h"
#include "Engine/World.h"

OctreeNode::OctreeNode()
{
	nodePos = FVector::ZeroVector;
	spilt = false;
	spawnedChunksCompleted = 0;
}

OctreeNode::OctreeNode(FVector newNodePos, APlanetController* newPlanetController, float newNodeVertexDistance, int newOctreeLevel)
{
	nodePos = newNodePos;
	meshCentre = nodePos;
	planetController = newPlanetController;
	parentNode = nullptr;
	nodeVertexDistance = newNodeVertexDistance;
	octreeLevel = newOctreeLevel;
	spilt = false;
	nodeMarkedForRemoving = false;
	spawnedChunksCompleted = 0;
	nodeTargetDistance = 4000000;
	SpawnChunk();
}

OctreeNode::OctreeNode(FVector newNodePos, APlanetController* newPlanetController, OctreeNode* newParentNode, float newNodeVertexDistance
	, int newOctreeLevel, int newNodeIndex, float newNodeTargetDistance)
{
	nodePos = newNodePos;
	meshCentre = nodePos;
	planetController = newPlanetController;
	parentNode = newParentNode;
	nodeVertexDistance = newNodeVertexDistance;
	octreeLevel = newOctreeLevel;
	nodeIndex = newNodeIndex;
	spilt = false;
	nodeMarkedForRemoving = false;
	spawnedChunksCompleted = 0;
	nodeTargetDistance = newNodeTargetDistance;
	SpawnChunk();
}

void OctreeNode::RemoveNode()
{
	GLog->Log("Remove Node");
	//remove the chunk
	//if (nodeChunk) { nodeChunk->Destroy(); }
	//remove this node
	if (nodeIndex <= 0)
	{
		planetController->AddChunkToDestroy(nodeChunk);
		//nodeChunk = nullptr;
		//nodeHasChunk = false;
		if (planetController) { planetController->RemoveNode(nodePos); }
	}
	else
	{
		if (parentNode) 
		{ 
			//setting vaules
			//parentNode->spawnedChunksCompleted -= 1;
			planetController->AddChunkToDestroy(nodeChunk);
			nodeChunk = nullptr;
			//fianlly remove node from parent
			parentNode->RemoveNodeFromParent(nodeIndex); 			
		}
	}
	
}

void OctreeNode::RemoveAllNodes()
{
	if (nodes.Num() == 0) { return; }
	TArray<OctreeNode*> nodesPtr;
	nodes.GenerateValueArray(nodesPtr);
	for (int i = 0; i < nodesPtr.Num();i++)
	{
		if (nodesPtr[i])
		{
			nodesPtr[i]->RemoveAllNodes();
			planetController->AddChunkToDestroy(nodesPtr[i]->nodeChunk);
			nodesPtr[i]->nodeChunk = nullptr;
			RemoveNodeFromParent(nodesPtr[i]->nodeIndex);
			//destory nodes
			//if (nodesPtr[i]->nodeHasChunk) { nodesPtr[i]->nodeChunk->Destroy(); }
		}
		//nodesPtr[i]->RemoveNode();
	}
	spilt = false;
	spawnedChunksCompleted = 0;
	//planetController->AddChunkToDestroy(nodeChunk);
	nodes.Empty();
}

void OctreeNode::RemoveNodeFromParent(int removeNodeIndex)
{
	OctreeNode* currentNode = nodes.FindRef(removeNodeIndex);
	if (currentNode) 
	{ 
		if (currentNode->spilt) { currentNode->RemoveAllNodes(); } 
		//if (currentNode->nodeChunk) { planetController->AddChunkToDestroy(currentNode->nodeChunk); }
	}
	nodes.Remove(removeNodeIndex);
	GLog->Log("Nodes in this octree remaining : " + FString::FromInt(nodes.Num()));
}

void OctreeNode::SpiltOctreeNode()
{
	if (spilt) { return; }
	spilt = true;
	float halfVertexDistance = nodeVertexDistance / 2;
	float quatChunkSize = (nodeVertexDistance * 25) / 4;
	int newOctreeLevel = octreeLevel += 1;
	float newNodeTargetDistance = nodeTargetDistance / 2;

	FVector newNodePos = FVector(nodePos.X - quatChunkSize, nodePos.Y - quatChunkSize, nodePos.Z - quatChunkSize);
	nodes.Add(0, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 0, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X + quatChunkSize, nodePos.Y - quatChunkSize, nodePos.Z - quatChunkSize);
	nodes.Add(1, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 1, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X - quatChunkSize, nodePos.Y + quatChunkSize, nodePos.Z - quatChunkSize);
	nodes.Add(2, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 2, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X + quatChunkSize, nodePos.Y + quatChunkSize, nodePos.Z - quatChunkSize);
	nodes.Add(3, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 3, newNodeTargetDistance));
	
	newNodePos = FVector(nodePos.X - quatChunkSize, nodePos.Y - quatChunkSize, nodePos.Z + quatChunkSize);
	nodes.Add(4, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 4, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X + quatChunkSize, nodePos.Y - quatChunkSize, nodePos.Z + quatChunkSize);
	nodes.Add(5, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 5, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X - quatChunkSize, nodePos.Y + quatChunkSize, nodePos.Z + quatChunkSize);
	nodes.Add(6, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 6, newNodeTargetDistance));

	newNodePos = FVector(nodePos.X + quatChunkSize, nodePos.Y + quatChunkSize, nodePos.Z + quatChunkSize);
	nodes.Add(7, new OctreeNode(newNodePos, planetController, this, halfVertexDistance, newOctreeLevel, 7, newNodeTargetDistance));
}

void OctreeNode::UpdateOctreeNode(FVector playerPos)
{
	//check if this node has been spilt
	if (spilt)
	{
		//check if all the chunks have been calcaulted 
		//GLog->Log("completed spawn chunks: " + FString::FromInt(spawnedChunksCompleted));
		if (spawnedChunksCompleted >= 8 && nodeHasChunk && !nodeMarkedForRemoving)
		{
			GLog->Log("Remove chunk mesh!!!");
			//can remove this nodes chunk as chunks below
			if (nodeChunk) 
			{ 
				//nodeChunk->Destroy(); 
				planetController->AddChunkToDestroy(nodeChunk);
				nodeChunk = nullptr;
				nodeHasChunk = false;
			}
		}

		float dis = FVector::Distance(playerPos, meshCentre);
		if (dis > (nodeTargetDistance + (nodeVertexDistance * 25)))
		{
			if (nodeHasChunk && nodeChunkFinished)
			{
				GLog->Log("RemoveAll Bodes!!!");
				RemoveAllNodes();
				spilt = false;
				nodeMarkedForRemoving = false;
			}
			else if(!nodeHasChunk) 
			{ 
				GLog->Log("Gen New Mesh!!");
				nodeMarkedForRemoving = true;
				SpawnChunk(); 
			}					
		}

		//update child nodes
		TArray<OctreeNode*> sNodes;
		nodes.GenerateValueArray(sNodes);
		for (int i = 0; i < sNodes.Num();i++)
		{
			sNodes[i]->UpdateOctreeNode(playerPos);		
		}
	}
	//should this node spilt?
	else
	{
		//if(!nodeHasChunk)
		//{
		//	GLog->Log("Gen New Mesh!!");
		//	//nodeMarkedForRemoving = true;
		//	SpawnChunk();
		//}
		if (nodeTargetDistance < 10000) { return; }
		float dis = FVector::Distance(playerPos, meshCentre);
	//	GLog->Log("Distance betwen non spilt nodes : " + FString::FromInt(dis) + "Targert distance : " + FString::FromInt(nodeTargetDistance));
		if (dis < (nodeTargetDistance))
		{
			GLog->Log("Auto spilt");
			SpiltOctreeNode();
		}
	}
}

void OctreeNode::RemoveLOD()
{
	TArray<OctreeNode*> sNodes;
	nodes.GenerateValueArray(sNodes);
	for (int i = 0; i < sNodes.Num();i++)
	{
		//outside distaance so remove nodes 
		//before removing node check parent node has mesh
		if (nodeHasChunk && nodeChunkFinished) { sNodes[i]->RemoveNode(); }
		else if (!nodeHasChunk) { SpawnChunk(); }
	}
}

void OctreeNode::SpawnChunk()
{	
	if (planetController != nullptr)
	{
		//spawn chunk
		GLog->Log("planet controller");
		nodeHasChunk = true;
		nodeChunkFinished = false;
		nodeChunk = planetController->SpawnChunk(this, nodePos, nodeVertexDistance);
	}
	else
	{
		GLog->Log("No planet controller");
	}
}


